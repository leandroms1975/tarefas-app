<?php

	$acao = 'recuperarTarefasPendentes';
	require 'tarefa_controller.php';

?>

<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>App Lista Tarefas</title>

		<link rel="stylesheet" href="css/estilo2.css">
        <link rel="script" type="text/javascript" href="js/script.js">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	</head>

	<body>

			<div class="container app">
				<div class="row">
					<div class="col-md-3 menu">
						<ul class="list-group">
							<li class="list-group-item"><a onclick="openPage('1', 'page')">Tarefas pendentes</a></li>
							<li class="list-group-item active"><a onclick="openPage('2', 'page')">Nova tarefa</a></li>
							<li class="list-group-item"><a onclick="openPage('3', 'page')">Todas tarefas</a></li>
						</ul>
					</div>

						<div class="col-md-9">

							<div class="container pagina">
								<div class="row">
									<div class="col">
										<h4>Nova tarefa</h4>
										<hr />

										<form method="post" action="tarefa_controller.php?acao=inserir">
											<div class="form-group">
												<label>Descrição da tarefa:</label>
												<input type="text" class="form-control" name="tarefa" placeholder="Exemplo: Lavar o carro">
											</div>

											<button class="btn btn-success">Cadastrar</button>
										</form>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

	</body>
</html>